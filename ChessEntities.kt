
interface Piece {
    val side: Side
    val p: Position
    val img: String
    val moved: Boolean
    val searchKey: PType
    val family: PFamily
    val value: Int
    fun allMoves(board: Board): List<Position>
    fun move(p:Position): Piece
    fun stringRepr() = "$side's $family @ $p"
}

class Queen(override val side: Side, override val p: Position, override val img: String,
            override val moved: Boolean, override val searchKey: PType, override val family: PFamily, override val value: Int = 9) : Piece {
    override fun move(p: Position): Piece {
        return Queen(side, p, img, true, searchKey, family)
    }


    override fun allMoves(board: Board): List<Position> {
        val bitboardsmoves = MoveGenerator.forQueen(p,
            if (side == Side.B) board.bitboard.white.all else board.bitboard.black.all,
            if (side == Side.W) board.bitboard.white.all else board.bitboard.black.all)
        return Position.fromBitBoard(bitboardsmoves)
    }
    override fun toString() = super.stringRepr()
}

class Knight(override val side: Side, override val p: Position, override val img: String,
             override val moved: Boolean, override val searchKey: PType, override val family: PFamily, override val value: Int = 3) : Piece {
    override fun move(p: Position): Piece {
        return Knight(side, p, img, true, searchKey, family)
    }


    override fun allMoves(board: Board): List<Position> {
        val bitboardsmoves = MoveGenerator.forKnight(p, board.bitboard.free,
            if (side == Side.B) board.bitboard.white.all else board.bitboard.black.all)
        return Position.fromBitBoard(bitboardsmoves)
    }
    override fun toString() = super.stringRepr()
}

class Rook(override val side: Side, override val p: Position, override val img: String,
           override val moved: Boolean, override val searchKey: PType, override val family: PFamily, override val value: Int = 5) : Piece {
    override fun move(p: Position): Piece {
        return Rook(side, p, img, true, searchKey, family)
    }

    override fun allMoves(board: Board): List<Position> {
        val bitboardsmoves = MoveGenerator.forRook(p,
            if (side == Side.B) board.bitboard.white.all else board.bitboard.black.all,
            if (side == Side.B) board.bitboard.black.all else board.bitboard.white.all)
        return Position.fromBitBoard(bitboardsmoves)
    }
    override fun toString() = super.stringRepr()
}

class Bishop(override val side: Side, override val p: Position, override val img: String,
             override val moved: Boolean, override val searchKey: PType, override val family: PFamily, override val value: Int = 3) : Piece {
    override fun move(p: Position) = Bishop(side, p, img, true, searchKey, family)

    override fun allMoves(board: Board): List<Position> {
        val bitboardsmoves = MoveGenerator.forBishop(p,
            if (side == Side.B) board.bitboard.white.all else board.bitboard.black.all,
            if (side == Side.B) board.bitboard.black.all else board.bitboard.white.all)
        return Position.fromBitBoard(bitboardsmoves)
    }
    override fun toString() = super.stringRepr()
}


class King(override val side: Side, override val p: Position, override val img: String,
           override val moved: Boolean, override val searchKey: PType, override val family: PFamily, override val value: Int = 0) : Piece {
    override fun move(p: Position) = King(side, p, img, true, searchKey, family)

    private fun leftCastlingMove(board: Board): Position? {
        val rook = board.pieces.v(side)[PType.ROOK_L]
        val mask = if (side == Side.B) Masks.MASK_B_NO_PIECES_BETWEEN_KING_AND_ROOK_L else Masks.MASK_W_NO_PIECES_BETWEEN_KING_AND_ROOK_L
        if ((board.bitboard.all and mask) == 0uL && rook!=null && !rook.moved && !moved) {
            return if (side == Side.B) Position(0,0) else Position(0,7)
        }
        return null
    }

    private fun rightCastlingMove(board: Board): Position? {
        val rook = board.pieces.v(side)[PType.ROOK_R]
        val mask = if (side == Side.B) Masks.MASK_B_NO_PIECES_BETWEEN_KING_AND_ROOK_R else Masks.MASK_W_NO_PIECES_BETWEEN_KING_AND_ROOK_R
        if ((board.bitboard.all and mask) == 0uL && rook!=null && !rook.moved && !moved) {
            return if (side == Side.B) Position(7,0) else Position(7,7)
        }
        return null
    }

    override fun allMoves(board: Board): List<Position> {
        val bitboardsmoves = MoveGenerator.forKing(p, board.bitboard, side)

        val move1 = leftCastlingMove(board)
        val move2 = rightCastlingMove(board)

        return Position.fromBitBoard(bitboardsmoves).plusElementIfNotNull(move1).plusElementIfNotNull(move2)
    }
    fun inCheck(board: Board) = MoveGenerator.inCheck(this, board.bitboard)
    override fun toString() = super.stringRepr()
}

class Pawn(override val side: Side, override val p: Position, override val img: String,
           override val moved: Boolean, override val searchKey: PType, override val family: PFamily, override val value: Int = 1) : Piece {
    override fun move(p: Position) = MoveGenerator.ask4PawnPromotion(Pawn(side, p, img, true, searchKey, family)).piece

    override fun allMoves(board: Board): List<Position> {
        val bitboardsmoves = MoveGenerator.forPawn(p, side, moved, board.bitboard.free,
            if (side == Side.W) board.bitboard.black.all else board.bitboard.white.all)
        return Position.fromBitBoard(bitboardsmoves)
    }
    override fun toString() = super.stringRepr()
}
