import kotlin.random.Random
import java.lang.Exception

enum class Side(val nr: Int) {
    W(-1), B(1);
    fun other() = if (this == W) B else W
    companion object { fun random() = listOf(W, B).random() }
}

data class Position(val x: Int, val y: Int) {
    fun bitboardIndex() = y * 8 + x
    override fun hashCode() = (x * 31) + y
    override fun toString() = "$x.$y"
    override fun equals(other: Any?): Boolean {
        if (other == null) return false
        if (other !is Position) return false
        return (other.x == x) and (other.y == y)
    }

    companion object {
        fun fromString(str: String, spliterator: Char='.'): Position {
            val (x, y) = str.split(spliterator)
            return Position(x.toInt(), y.toInt())
        }
        fun fromBitBoard(b: ULong, result: List<Position> = emptyList()): List<Position> {
            if (b == 0uL)
                return result
            val len = b.countLeadingZeroBits()
            val x = len % 8
            val y = len / 8
            val p = Position(x, y)
            return fromBitBoard(b.minus(b.takeHighestOneBit()), result.plusElement(p))
        }
    }
}
data class Move(val piece: Piece, val to: Position)
enum class ChessStatus { RUNNING, DRAW, FIFTY_MOVE, CHECK_MATE, STALE_MATE, WHITE_IN_CHECK, BLACK_IN_CHECK, INVALID }
class ChessException(msg: String): Exception(msg)

val zobristValues = (0 .. 64).map {
    mapOf(
        Side.B to mapOf(PFamily.PAWN to Random.nextLong(), PFamily.ROOK to Random.nextLong(), PFamily.KNIGHT to Random.nextLong(), PFamily.BISHOP to Random.nextLong(), PFamily.QUEEN to Random.nextLong(), PFamily.KING to Random.nextLong()),
        Side.W to mapOf(PFamily.PAWN to Random.nextLong(), PFamily.ROOK to Random.nextLong(), PFamily.KNIGHT to Random.nextLong(), PFamily.BISHOP to Random.nextLong(), PFamily.QUEEN to Random.nextLong(), PFamily.KING to Random.nextLong()
        ))
}