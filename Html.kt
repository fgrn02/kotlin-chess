object Html {

    fun apiHtmlRepr(board: Board, movesToShow: List<Position> = emptyList(), additionalParameters: Map<String, String> = emptyMap()): Map<String, Any> = with(board) {

        val hashMap = hashMapOf(*Html.chessboard)

        pieces.v(turn).values.forEach { piece ->
            val cell : Html.Cell = hashMap.v(piece.p)
            hashMap[piece.p] = cell.setImg(piece.img).addOnClick("showMoves(${piece.p.x},${piece.p.y})")
        }
        pieces.v(turn.other()).values.forEach { piece ->
            val cell : Html.Cell = hashMap.v(piece.p)
            hashMap[piece.p] = cell.setImg(piece.img)
        }
        movesToShow.forEach { p ->
            val cell : Html.Cell = hashMap.v(p)
            hashMap[p] = cell.setClass("show").addOnClick("move(${p.x}, ${p.y})")
        }
        return mapOf(
            "board" to Html.Board8.fromMap(hashMap),
            "chessResult" to mapOf(
                ChessStatus.DRAW to "it's a draw",
                ChessStatus.FIFTY_MOVE to "it's a draw due to fifty move rule",
                ChessStatus.CHECK_MATE  to  (if (turn == Side.W) "Check mate. black won" else "Check mate. white won"),
                ChessStatus.STALE_MATE  to  (if (turn == Side.W) "Stale mate. black won" else "Stale mate. white won"),
                ChessStatus.BLACK_IN_CHECK to "black is in check",
                ChessStatus.WHITE_IN_CHECK to "white is in check",
                ChessStatus.RUNNING to "",
                ChessStatus.INVALID to "invalid move"
            ).v(status),
            "bitboard" to bitboard.all.toMatrix(),
            "turn" to turn,
            "status" to status.toString(),
            "fifty" to fiftyMoveRule
        ) + additionalParameters
    }

    private fun color(x: Int, y: Int) = if ((x +y) % 2 == 0) "white" else "black"

    class Cell(private val x: Int, private val y: Int, private val onclick: List<String> = emptyList(), private val img: String = "", private val className: String = color(x, y)) {

        override fun toString(): String {
            return "<img " + " " +
                    "id=\"$x.$y\"" + " " +
                    "class=\"$className\"" + " " +
                    (if (onclick.isEmpty()) "" else "onclick=\"${onclick.joinToString(";")}\"") + " " +
                    (if (img.isEmpty()) "" else "src=\"$img\"") + " " +
                    ">"
        }
        fun addOnClick(add: String): Cell = Cell(x, y, onclick.plusElement(add), img, className)
        fun setImg(img: String): Cell = Cell(x, y, onclick, img, className)
        fun setClass(className: String): Cell = Cell(x, y, onclick, img, className)
    }

    val chessboard = (0 .. 7).map { y -> (0 .. 7)
        .map { x ->
            Position(x, y) to Cell(x, y)
        }}.flatten().toTypedArray()

    data class Board8(val row1: String, val row2 : String, val row3 : String, val row4 : String,
                      val row5 : String, val row6 : String, val row7 : String, val row8: String) {
        companion object {
            fun fromMap(hashMap: HashMap<Position, Cell>): Board8 {
                    val rows = hashMap.toSortedMap(
                        Comparator { p1, p2 ->
                            if (p1.x + p1.y*100 > p2.x + p2.y*100) 1 else -1
                        }
                    ).values.chunked(8).map { it.joinToString("\n") }
                return Board8(rows[0], rows[1], rows[2], rows[3], rows[4], rows[5], rows[6], rows[7])
                }
            }
    }
}


class Images {
    object B {
        const val KING = "http://localhost:7000/king_b.png"
        const val QUEEN = "http://localhost:7000/queen_b.png"
        const val ROOK = "http://localhost:7000/rook_b.png"
        const val BISHOP = "http://localhost:7000/bishop_b.png"
        const val KNIGHT = "http://localhost:7000/knight_b.png"
        const val PAWN = "http://localhost:7000/pawn_b.png"
    }
    object W {
        const val KING = "http://localhost:7000/king_w.png"
        const val QUEEN = "http://localhost:7000/queen_w.png"
        const val ROOK = "http://localhost:7000/rook_w.png"
        const val BISHOP = "http://localhost:7000/bishop_w.png"
        const val KNIGHT = "http://localhost:7000/knight_w.png"
        const val PAWN = "http://localhost:7000/pawn_w.png"
    }
}

//class Images {
//    object B {
//        const val KING = "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f0/Chess_kdt45.svg/500px-Chess_kdt45.svg.png"
//        const val QUEEN = "https://upload.wikimedia.org/wikipedia/commons/thumb/4/47/Chess_qdt45.svg/500px-Chess_qdt45.svg.png"
//        const val ROOK = "https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Chess_rdt45.svg/500px-Chess_rdt45.svg.png"
//        const val BISHOP = "https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Chess_bdt45.svg/500px-Chess_bdt45.svg.png"
//        const val KNIGHT = "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ef/Chess_ndt45.svg/500px-Chess_ndt45.svg.png"
//        const val PAWN = "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c7/Chess_pdt45.svg/500px-Chess_pdt45.svg.png"
//    }
//    object W {
//        const val KING = "https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/Chess_klt45.svg/500px-Chess_klt45.svg.png"
//        const val QUEEN = "https://upload.wikimedia.org/wikipedia/commons/thumb/1/15/Chess_qlt45.svg/500px-Chess_qlt45.svg.png"
//        const val ROOK = "https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Chess_rlt45.svg/500px-Chess_rlt45.svg.png"
//        const val BISHOP = "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b1/Chess_blt45.svg/500px-Chess_blt45.svg.png"
//        const val KNIGHT = "https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Chess_nlt45.svg/500px-Chess_nlt45.svg.png"
//        const val PAWN = "https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/Chess_plt45.svg/500px-Chess_plt45.svg.png"
//    }
//}
