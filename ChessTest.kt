
object ChessTest {

    // Test 1: CPU kann im naechstem Zug gewinnen, erkennt den Zug, und spielt diesen.
    // Scenario: Koenig in letzte Reiher von Turm festgezetzt. Dame setzt ihn im naehcsten Zug schachmatt.
    object Test1 {
        val board: Board
        get() {
            val pieces = mapOf(
                Side.W to mapOf(
                    PType.KING to     King(Side.W, Position(0,0), Images.W.KING, true, PType.KING, PFamily.KING),
                    PType.QUEEN to Queen(Side.W, Position(5,4), Images.W.QUEEN, true, PType.QUEEN, PFamily.QUEEN),
                    PType.ROOK_R to   Rook(Side.W, Position(0,6), Images.W.ROOK, true, PType.ROOK_R, PFamily.ROOK)
                ),
                Side.B to mapOf(
                    PType.KING to     King(Side.B, Position(7,7), Images.B.KING, true, PType.KING, PFamily.KING)
                )
            )
            return Board(Side.W,null, pieces, BitBoard.fromPieces(pieces), ChessStatus.RUNNING, 0)
        }
    }


    // Test 2: CPU kann im uebernaechstem Zug gewinnen, erkennt den Zug, und spielt diesen.
    // Scenario: Weiss in Schach von Turm. Koenig muss in naechstem Zug in letzte Reihe gehen.
    // Im uebernaehcsten Zug, setzt die schwarze Dame ihn schachmatt.
    object Test2 {
        val board : Board
        get() {
            val pieces = mapOf(
                Side.B to mapOf(
                    PType.KING to     King(Side.B, Position(0,0), Images.B.KING, true, PType.KING, PFamily.KING),
                    PType.QUEEN to Queen(Side.B, Position(1,5), Images.B.QUEEN, true, PType.QUEEN, PFamily.QUEEN),
                    PType.ROOK_R to   Rook(Side.B, Position(0,6), Images.B.ROOK, true, PType.ROOK_R, PFamily.ROOK)
                ),
                Side.W to mapOf(
                    PType.KING to     King(Side.W, Position(7,7), Images.W.KING, true, PType.KING, PFamily.KING)//,
                   // PType.KNIGHT_L to Knight(Side.W, Position(5,5), Images.W.KNIGHT, true, PType.KNIGHT_L, PFamily.KNIGHT)
                )
            )
        return Board(Side.W,null, pieces, BitBoard.fromPieces(pieces), ChessStatus.RUNNING, 0)
        }
    }
    // Test 3: Die CPU erkennt, dass sie in in 3 Zuegen gewinnen.
    object Test3 {

        val board: Board
        get() { // todo...
            val pieces = mapOf(
                Side.B to mapOf(
                    PType.KING to     King(Side.B, Position(0,1), Images.B.KING, false, PType.KING, PFamily.KING),
                    PType.PAWN_0 to     Pawn(Side.B, Position(1,1), Images.B.PAWN, false, PType.PAWN_0, PFamily.PAWN),
                    PType.PAWN_2 to     Pawn(Side.B, Position(1,2), Images.B.PAWN, false, PType.PAWN_2, PFamily.PAWN),
                    PType.PAWN_1 to     Pawn(Side.B, Position(3,0), Images.B.PAWN, false, PType.PAWN_1, PFamily.PAWN),
                    PType.PAWN_3 to     Pawn(Side.B, Position(2,1), Images.B.PAWN, false, PType.PAWN_3, PFamily.PAWN),
                    PType.QUEEN to Queen(Side.B, Position(0,0), Images.B.QUEEN, false, PType.QUEEN, PFamily.QUEEN),
                    PType.ROOK_R to   Rook(Side.B, Position(0,6), Images.B.ROOK, false, PType.ROOK_R, PFamily.ROOK)
                ),
                Side.W to mapOf(
                    PType.KING to     King(Side.W, Position(7,7), Images.W.KING, false, PType.KING, PFamily.KING)
                )
            )

            return Board(Side.B,null, pieces, BitBoard.fromPieces(pieces), ChessStatus.RUNNING, 0)
        }
    }
    // Test 4: Die CPU erkennt, dass sie in zwei Zuegen schach matt sein kann, und verhindert es.
    // Scenario: der weisse koenig befindet sich hinter einem schild von bauern.
    // der weisse turm schleicht sich,um ihn zu flankieren und matt zu setzen. die einzige chance, die weiss bleibt, ist den knight davorzusetzen ...
    object Test4 {
        val board: Board
            get() {
                val pieces = mapOf(
                    Side.B to mapOf(
                        PType.KING to     King(Side.B, Position(4,0), Images.B.KING, true, PType.KING, PFamily.KING),
                        PType.ROOK_R to   Rook(Side.B, Position(0,4), Images.B.ROOK, true, PType.ROOK_R, PFamily.ROOK)
                    ),
                    Side.W to mapOf(
                        PType.KNIGHT_L to Knight(Side.W, Position(4,5), Images.W.KNIGHT, true, PType.KNIGHT_L, PFamily.KNIGHT),
                        PType.KNIGHT_R to Knight(Side.W, Position(5,5), Images.W.KNIGHT, true, PType.KNIGHT_R, PFamily.KNIGHT),
                        PType.PAWN_0 to     Pawn(Side.W, Position(3,6), Images.W.PAWN, true, PType.PAWN_0, PFamily.PAWN),
                        PType.PAWN_1 to     Pawn(Side.W, Position(4,6), Images.W.PAWN, true, PType.PAWN_1, PFamily.PAWN),
                        PType.PAWN_2 to     Pawn(Side.W, Position(5,6), Images.W.PAWN, true, PType.PAWN_2, PFamily.PAWN),
                        PType.PAWN_3 to     Pawn(Side.W, Position(6,6), Images.W.PAWN, true, PType.PAWN_3, PFamily.PAWN),
                        PType.PAWN_4 to     Pawn(Side.W, Position(2,6), Images.W.PAWN, true, PType.PAWN_4, PFamily.PAWN),
                        PType.PAWN_5 to     Pawn(Side.W, Position(3,5), Images.W.PAWN, true, PType.PAWN_5, PFamily.PAWN),
                        PType.KING to     King(Side.W, Position(4,7), Images.W.KING, true, PType.KING, PFamily.KING)
                    )
                )
                return Board(Side.B,null, pieces, BitBoard.fromPieces(pieces), ChessStatus.RUNNING, 0)
            }
    }
    // black is about to pawn promote to queen and set white checkmate or at least stalemate. whites king has to prevent the promotion.
    object Test5 {
        val board: Board
            get() {
                val pieces = mapOf(
                    Side.B to mapOf(
                        PType.KING to     King(Side.B, Position(0,0), Images.B.KING, true, PType.KING, PFamily.KING),
                        PType.ROOK_L to Rook(Side.B, Position(0,6), Images.B.ROOK, true, PType.ROOK_L, PFamily.ROOK),
                        PType.PAWN_0 to     Pawn(Side.B, Position(5,5), Images.B.PAWN, true, PType.PAWN_0, PFamily.PAWN)
                    ),
                    Side.W to mapOf(
                        PType.KING to     King(Side.W, Position(7,7), Images.W.KING, true, PType.KING, PFamily.KING)
                    )
                )
                return Board(Side.W,null, pieces, BitBoard.fromPieces(pieces), ChessStatus.RUNNING, 0)
            }
    }

}