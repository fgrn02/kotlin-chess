object Evaluator {
    // white pieces:
    // - (black pieces are just reversed)
    val pawn = listOf(
        0,  0,  0,  0,  0,  0,  0,  0,
        50, 50, 50, 50, 50, 50, 50, 50,
        10, 10, 20, 30, 30, 20, 10, 10,
        5,  5, 10, 25, 25, 10,  5,  5,
        0,  0,  0, 20, 20,  0,  0,  0,
        5, -5,-10,  0,  0,-10, -5,  5,
        5, 10,-10,-20,-20,-10, 10,  5,
        0,  0,  0,  0,  0,  0,  0,  0
    )
    val knight = listOf(
        -50,-40,-30,-30,-30,-30,-40,-50,
        -40,-20,  0,  0,  0,  0,-20,-40,
        -30,  0, 10, 15, 15, 10,  0,-30,
        -30,  5, 15, 20, 20, 15,  5,-30,
        -30,  0, 15, 20, 20, 15,  0,-30,
        -30,  5, 10, 15, 15, 10,  5,-30,
        -40,-20,  0,  5,  5,  0,-20,-40,
        -50,-40,-20,-30,-30,-20,-40,-50
    )
    val bishop = listOf(
        -20,-10,-10,-10,-10,-10,-10,-20,
        -10,  0,  0,  0,  0,  0,  0,-10,
        -10,  0,  5, 10, 10,  5,  0,-10,
        -10,  5,  5, 10, 10,  5,  5,-10,
        -10,  0, 10, 10, 10, 10,  0,-10,
        -10, 10, 10, 10, 10, 10, 10,-10,
        -10,  5,  0,  0,  0,  0,  5,-10,
        -20,-10,-40,-10,-10,-40,-10,-20
    )
    val king = listOf(
        -30, -40, -40, -50, -50, -40, -40, -30,
        -30, -40, -40, -50, -50, -40, -40, -30,
        -30, -40, -40, -50, -50, -40, -40, -30,
        -30, -40, -40, -50, -50, -40, -40, -30,
        -20, -30, -30, -40, -40, -30, -30, -20,
        -10, -20, -20, -20, -20, -20, -20, -10,
        20,  20,   0,   0,   0,   0,  20,  20,
        20,  30,  10,   0,   0,  10,  30,  20
    )
    val queen = listOf(
        -20,-10,-10, -5, -5,-10,-10,-20,
        -10,  0,  0,  0,  0,  0,  0,-10,
        -10,  0,  5,  5,  5,  5,  0,-10,
        -5,  0,  5,  5,  5,  5,  0, -5,
        0,  0,  5,  5,  5,  5,  0, -5,
        -10,  5,  5,  5,  5,  5,  0,-10,
        -10,  0,  5,  0,  0,  0,  0,-10,
        -20,-10,-10, -5, -5,-10,-10,-20
    )
    val rook = listOf(
        0,  0,  0,  0,  0,  0,  0,  0,
        5, 10, 10, 10, 10, 10, 10,  5,
        -5,  0,  0,  0,  0,  0,  0, -5,
        -5,  0,  0,  0,  0,  0,  0, -5,
        -5,  0,  0,  0,  0,  0,  0, -5,
        -5,  0,  0,  0,  0,  0,  0, -5,
        -5,  0,  0,  0,  0,  0,  0, -5,
        0,  0,  0,  5,  5,  0,  0,  0
    )
    val pieceSquareTables = mapOf(
        Side.B to mapOf(
            PFamily.PAWN to pawn.reversed(),
            PFamily.KNIGHT to knight.reversed(),
            PFamily.KING to king.reversed(),
            PFamily.QUEEN to queen.reversed(),
            PFamily.BISHOP to bishop.reversed(),
            PFamily.ROOK to rook.reversed()
        ),
        Side.W to mapOf(
            PFamily.PAWN to pawn,
            PFamily.KNIGHT to knight,
            PFamily.KING to king,
            PFamily.QUEEN to queen,
            PFamily.BISHOP to bishop,
            PFamily.ROOK to rook
        )
    )

    fun piecePositionValue(piece : Piece) = pieceSquareTables.v(piece.side).v(piece.family)[piece.p.bitboardIndex()]

    fun eval(board: Board): Int = with(board) {
        if ((status == ChessStatus.DRAW) or (status == ChessStatus.FIFTY_MOVE)) {
            return 0
        }
        if (status == ChessStatus.CHECK_MATE) {
            return turn.other().nr * 1_000_000_000
        }
        if (status == ChessStatus.STALE_MATE) {
            return turn.nr
        }

        val checkBonus =  if (turn==Side.B) (ChessStatus.WHITE_IN_CHECK==status).toInt(0) else (ChessStatus.BLACK_IN_CHECK==status).toInt(0)
        val pieceValueBonus = ((pieces.v(turn).values.sumBy { it.value }) - (pieces.v(turn.other()).values.sumBy { it.value })) * 1000
        val tablesValue = pieces.v(turn).map { piecePositionValue(it.value) }.sum()
        return (pieceValueBonus + tablesValue + checkBonus) * turn.nr
        // other evaluation criteria from chessprograming_wiki, not sure about
//        val myBitBoard = bitboard.get4Side(turn)
//        val opBitBoard = bitboard.get4Side(turn.other())
//        val kingArea = (myBitBoard.king or
//                ((myBitBoard.king shr 1) and Masks.COL0_TO_COL6) or  // right
//                ((myBitBoard.king shl 1) and Masks.COL1_TO_COL7) or  // left
//                ((myBitBoard.king shr 8) and Masks.ROW0_TO_ROW6) or  // down
//                ((myBitBoard.king shl 8) and Masks.ROW1_TO_ROW7) or  // up
//                ((myBitBoard.king shl 7) and Masks.COL0_TO_COL6 and Masks.ROW1_TO_ROW7) or  // up-right
//                ((myBitBoard.king shr 9) and Masks.ROW0_TO_ROW6 and Masks.ROW0_TO_ROW6) or  // down-right
//                ((myBitBoard.king shl 9) and Masks.COL1_TO_COL7 and Masks.ROW1_TO_ROW7) or  // up-left
//                ((myBitBoard.king shr 7) and Masks.COL1_TO_COL7 and Masks.ROW0_TO_ROW6))     // down-left
//        val pawnShieldBonus = (kingArea and myBitBoard.pawns).countOneBits()
//        val pawnStormPenalty = -(kingArea and opBitBoard.pawns).countOneBits()
//        val virtualMobilityPenalty =
//            -Queen(turn, getKing().p, "", true, PType.QUEEN, PFamily.QUEEN).allMoves(this).count()
//        val checkBonus =
//            if (((turn == Side.B) and (status == ChessStatus.WHITE_IN_CHECK)) or ((turn == Side.W) and (status == ChessStatus.BLACK_IN_CHECK))) 5 else 0
//        val checkPenalty =
//            if (((turn == Side.B) and (status == ChessStatus.BLACK_IN_CHECK)) or ((turn == Side.W) and (status == ChessStatus.WHITE_IN_CHECK))) -5 else 0
//        val pieceValueBonus = (pieces.v(turn).values.sumBy { it.value } - pieces.v(turn.other()).values.sumBy { it.value }) * 500
//        val pieceMobilityBonus = pieces.v(turn).values.map { it.allMoves(this).count() }.sum() / 2
//        val kingMovedPenalty = getKing().moved.toInt(0) * -5
//        return ((pieceValueBonus + pieceMobilityBonus + checkBonus + checkPenalty + pawnShieldBonus + pawnStormPenalty + virtualMobilityPenalty + kingMovedPenalty) * turn.nr)

    }
}