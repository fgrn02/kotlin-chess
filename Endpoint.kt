import MoveGenerator.START
import io.javalin.Javalin
import io.javalin.core.JavalinConfig
import java.util.function.Consumer

class Endpoint {

    private val chessApi = ChessApi()
    private val PORT = 7000
    private val config : Consumer<JavalinConfig> = Consumer {
        it.addStaticFiles("html")
        it.addStaticFiles("img")
    }

    private val app = Javalin.create(config)

    fun serve() {

        app.get("/memo/write") { ctx ->
            ChessAIWithNodes.persist()
            ctx.json(nodesMemo)
        }

        app.get("/board") { ctx ->  ctx.json(chessApi.apiHtmlRepr()) }
        app.get("/board/moves/:x/:y") { ctx ->
            chessApi.selectPiece(ctx.pathParam("x"), ctx.pathParam("y"))
            ctx.json(chessApi.apiHtmlRepr(chessApi.apiMoves4Piece()))

        }
        app.get("/board/move/:x/:y") { ctx ->
            val mode = ctx.queryParam("mode", "Test")
            chessApi.apiMakeMove(ctx.pathParam("x"), ctx.pathParam("y"))
            ctx.json(chessApi.apiHtmlRepr(emptyList()) + mapOf("mode" to mode))
        }
        app.get("/board/back") { ctx ->
            chessApi.apiRevokeMove()
            ctx.json(chessApi.apiHtmlRepr())
        }
        app.get("/board/random/:redo") { ctx ->
            val redo = ctx.pathParam("redo").toInt()
            redo times {
                chessApi.apiRandomlyPlayedBoard { ctx.json(chessApi.apiHtmlRepr()) }
                ctx.json(chessApi.apiHtmlRepr())
            }
        }
        app.get("/board/random") { ctx ->
            chessApi.apiRandomlyPlayedBoard { ctx.json(chessApi.apiHtmlRepr()) }
            ctx.json(chessApi.apiHtmlRepr())
        }
        app.get("/loadtest/:test") { ctx ->
            when (ctx.pathParam("test")) {
                "1" -> { chessApi.replaceBoard(ChessTest.Test1.board); chessApi.cpuDepth=1; ctx.json(chessApi.apiHtmlRepr()) }
                "2" -> { chessApi.replaceBoard(ChessTest.Test2.board); chessApi.cpuDepth=3; ctx.json(chessApi.apiHtmlRepr()) }
                "3" -> { chessApi.replaceBoard(ChessTest.Test3.board); chessApi.cpuDepth=5; ctx.json(chessApi.apiHtmlRepr()) }
                "4" -> { chessApi.replaceBoard(ChessTest.Test4.board); chessApi.cpuDepth=2; ctx.json(chessApi.apiHtmlRepr()) }
                "5" -> { chessApi.replaceBoard(ChessTest.Test5.board); chessApi.cpuDepth=4; ctx.json(chessApi.apiHtmlRepr()) }
            }
        }
        app.get("/board/new") { ctx ->
            val first = ctx.queryParam("first", "YOU")
            chessApi.cpuDepth=3
            chessApi.resetBoard()
            if (first!!.toUpperCase() == "CPU") {
                chessApi.apiMakeBestMove()
                ctx.json(chessApi.apiHtmlRepr(additionalParameters = mapOf("time" to "${chessApi.lastCPUsMoveFindTime} seconds", "move" to "${chessApi.lastCPUsMove?.piece} moved to ${chessApi.lastCPUsMove?.to}")))
            } else
                ctx.json(chessApi.apiHtmlRepr())
        }
        app.get("/board/move/random") { ctx ->
            chessApi.apiMakeRandomMove()
            ctx.json(chessApi.apiHtmlRepr())
        }
        app.get("/board/move/best") { ctx ->
            chessApi.apiMakeBestMove()
            ctx.json(chessApi.apiHtmlRepr(additionalParameters = mapOf("time" to "${chessApi.lastCPUsMoveFindTime} seconds", "move" to "${chessApi.lastCPUsMove?.piece} moved to ${chessApi.lastCPUsMove?.to}")))
        }
        // debug
        app.get("debug/moves/:x/:y") { ctx ->
            ctx.json(chessApi.apiMoves4Piece(ctx.pathParam("x"), ctx.pathParam("y")))
        }
        app.get("debug/bitboards") { ctx ->
            ctx.json(chessApi.board.bitboard.white.pawns.toString(2))
        }

        app.start(PORT)
    }
}
fun main() {

    Endpoint().serve()
}
