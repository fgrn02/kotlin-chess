import MoveGenerator.START

class BitBoard(val black: Pieces, val white: Pieces) {
    val all: ULong = (black.all or white.all)
    val free: ULong = all.inv()
    val side = mapOf(
        Side.B to black,
        Side.W to white
    )
    fun get4Side(side: Side) = if (side == Side.W) white else black

    fun bitRemoved(piece: Piece?, removalSide: Side?): BitBoard {
        if (piece == null)
            return this
        if (removalSide == null)
            return this
        return if (removalSide == Side.B) {
            BitBoard(
                black = Pieces(
                    if (piece.family == PFamily.PAWN) MoveGenerator.removeCell(
                        piece.p,
                        black.pawns
                    ) else black.pawns,
                    if (piece.family == PFamily.ROOK) MoveGenerator.removeCell(
                        piece.p,
                        black.rooks
                    ) else black.rooks,
                    if (piece.family == PFamily.KNIGHT) MoveGenerator.removeCell(
                        piece.p,
                        black.knights
                    ) else black.knights,
                    if (piece.family == PFamily.BISHOP) MoveGenerator.removeCell(
                        piece.p,
                        black.bishops
                    ) else black.bishops,
                    if (piece.family == PFamily.QUEEN) MoveGenerator.removeCell(
                        piece.p,
                        black.queens
                    ) else black.queens,
                    if (piece.family == PFamily.KING) MoveGenerator.removeCell(
                        piece.p,
                        black.king
                    ) else black.king
                ), white = white
            )
        } else {
            BitBoard(
                black = black, white = Pieces(
                    if (piece.family == PFamily.PAWN) MoveGenerator.removeCell(
                        piece.p,
                        white.pawns
                    ) else white.pawns,
                    if (piece.family == PFamily.ROOK) MoveGenerator.removeCell(
                        piece.p,
                        white.rooks
                    ) else white.rooks,
                    if (piece.family == PFamily.KNIGHT) MoveGenerator.removeCell(
                        piece.p,
                        white.knights
                    ) else white.knights,
                    if (piece.family == PFamily.BISHOP) MoveGenerator.removeCell(
                        piece.p,
                        white.bishops
                    ) else white.bishops,
                    if (piece.family == PFamily.QUEEN) MoveGenerator.removeCell(
                        piece.p,
                        white.queens
                    ) else white.queens,
                    if (piece.family == PFamily.KING) MoveGenerator.removeCell(
                        piece.p,
                        white.king
                    ) else white.king
                )
            )
        }
    }


    fun bitUpdated(m: Move): BitBoard {
        val (piece, newPosition) = m
        return  if (piece.side == Side.W) {
            BitBoard(
                black = black, white = Pieces(
                    if (piece.family == PFamily.PAWN) MoveGenerator.setCellState(
                        piece.p,
                        newPosition,
                        white.pawns
                    ) else white.pawns,
                    if (piece.family == PFamily.ROOK) MoveGenerator.setCellState(
                        piece.p,
                        newPosition,
                        white.rooks
                    ) else white.rooks,
                    if (piece.family == PFamily.KNIGHT) MoveGenerator.setCellState(
                        piece.p,
                        newPosition,
                        white.knights
                    ) else white.knights,
                    if (piece.family == PFamily.BISHOP) MoveGenerator.setCellState(
                        piece.p,
                        newPosition,
                        white.bishops
                    ) else white.bishops,
                    if (piece.family == PFamily.QUEEN) MoveGenerator.setCellState(
                        piece.p,
                        newPosition,
                        white.queens
                    ) else white.queens,
                    if (piece.family == PFamily.KING) MoveGenerator.setCellState(
                        piece.p,
                        newPosition,
                        white.king
                    ) else white.king
                )
            )
        } else {
            BitBoard(
                black = Pieces(
                    if (piece.family == PFamily.PAWN) MoveGenerator.setCellState(
                        piece.p,
                        newPosition,
                        black.pawns
                    ) else black.pawns,
                    if (piece.family == PFamily.ROOK) MoveGenerator.setCellState(
                        piece.p,
                        newPosition,
                        black.rooks
                    ) else black.rooks,
                    if (piece.family == PFamily.KNIGHT) MoveGenerator.setCellState(
                        piece.p,
                        newPosition,
                        black.knights
                    ) else black.knights,
                    if (piece.family == PFamily.BISHOP) MoveGenerator.setCellState(
                        piece.p,
                        newPosition,
                        black.bishops
                    ) else black.bishops,
                    if (piece.family == PFamily.QUEEN) MoveGenerator.setCellState(
                        piece.p,
                        newPosition,
                        black.queens
                    ) else black.queens,
                    if (piece.family == PFamily.KING) MoveGenerator.setCellState(
                        piece.p,
                        newPosition,
                        black.king
                    ) else black.king
                ),
                white = white
            )
        }
    }

    class Pieces (val pawns: ULong, val rooks: ULong, val knights: ULong, val bishops: ULong, val queens: ULong, val king: ULong) {
        val all: ULong = (pawns or rooks or knights or bishops or queens or king)
    }

    companion object {
        fun fromPieces(pieces: Map<Side, Map<PType, Piece>>) = BitBoard(
            black = fromPieces(
                pieces,
                Side.B
            ), white = fromPieces(pieces, Side.W)
        )
        private fun fromPieces(pieces: Map<Side, Map<PType, Piece>>, side: Side) =
            Pieces(
                pieces.v(side).values.filter { it.family == PFamily.PAWN }
                    .map { START shr it.p.bitboardIndex() }.reduceOrNull { a, b -> a or b } ?: 0uL,
                pieces.v(side).values.filter { it.family == PFamily.ROOK }
                    .map { START shr it.p.bitboardIndex() }.reduceOrNull { a, b -> a or b } ?: 0uL,
                pieces.v(side).values.filter { it.family == PFamily.KNIGHT }
                    .map { START shr it.p.bitboardIndex() }.reduceOrNull { a, b -> a or b } ?: 0uL,
                pieces.v(side).values.filter { it.family == PFamily.BISHOP }
                    .map { START shr it.p.bitboardIndex() }.reduceOrNull { a, b -> a or b } ?: 0uL,
                pieces.v(side).values.filter { it.family == PFamily.QUEEN }
                    .map { START shr it.p.bitboardIndex() }.reduceOrNull { a, b -> a or b } ?: 0uL,
                pieces.v(side).values.filter { it.family == PFamily.KING }
                    .map { START shr it.p.bitboardIndex() }.reduceOrNull { a, b -> a or b } ?: 0uL
            )
    }
}