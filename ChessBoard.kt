import MoveGenerator.START


class Board(
    val turn: Side,
    private val predecessor: Board?,
    val pieces: Map<Side, Map<PType, Piece>>,
    val bitboard: BitBoard,
    val status: ChessStatus,
    val fiftyMoveRule: Int
)  {
    val zobristHash = (pieces.v(turn).map { zobristValues[it.value.p.bitboardIndex()].v(turn).v(it.value.family) } +
                       pieces.v(turn.other()).map { zobristValues[it.value.p.bitboardIndex()].v(turn.other()).v(it.value.family) })
        .reduce { a, b -> a xor b } xor turn.nr.toLong()

    companion object { fun create() = Board(Side.W, null, INIT_DATA_OF_PIECES, BitBoard.fromPieces(INIT_DATA_OF_PIECES), ChessStatus.RUNNING, 0) }

    fun withUpdatedStatus(): Board {
        if (fiftyMoveRule >= 50) { return Board(turn, predecessor, pieces, bitboard, ChessStatus.FIFTY_MOVE, fiftyMoveRule) }
        if ((bitboard.white.king or bitboard.black.king) == bitboard.all) { return Board(turn, predecessor, pieces, bitboard, ChessStatus.DRAW, fiftyMoveRule) }

        if (getKing().inCheck(this)) {
            if (!askStillMovesToMake()) { return Board(turn, predecessor, pieces, bitboard, ChessStatus.CHECK_MATE, fiftyMoveRule) }
            return if (turn == Side.B) Board(turn, predecessor, pieces, bitboard, ChessStatus.BLACK_IN_CHECK, fiftyMoveRule)
            else Board(turn, predecessor, pieces, bitboard, ChessStatus.WHITE_IN_CHECK, fiftyMoveRule) }
            else { if (!askStillMovesToMake()) return Board(turn, predecessor, pieces, bitboard, ChessStatus.STALE_MATE, fiftyMoveRule) }

        return Board(turn, predecessor, pieces, bitboard, ChessStatus.RUNNING, fiftyMoveRule)
    }


    fun bestMove(depth: Int=3): Pair<Move?, Float> {
        val start = System.nanoTime()
//        val ab = AlphaBetaMiniMax(this,3)
//        val best = ab.sim()
//        return Pair(best.m, executionTime)

        val node= ChessAIWithNodes.getMove(this, depth)
        val executionTime = (System.nanoTime() - start) / 1_000_000_000f
        println("bestMove : $node  vs. ${eval()}")

        return node.move to executionTime
    }

    private fun strikedPieceAt(p: Position): Piece? {
        val maybe = pieces.v(turn.other()).values.firstOrNull { it.p == p }
        // should not happen anymore
        if (maybe is King) {
            println("strikedPieceAt($p). was King ??!.$status Illegal.!!!!!!\n".repeat(1))
            throw ChessException("strikedPieceAt($p). was King ??!.$status Illegal.!!!!!!\n".repeat(5))
            return null
        }
        return maybe
    }

    private fun castled(pType: PType, dir: Char): Board {
        val king = getKing()
        val rook = pieces.v(turn).v(pType)
        val (newKingPosition, newRookPosition) = MoveGenerator.kingCastling.v(turn).v(dir)
        return Board(turn.other(), this,
            mapOf(
                turn to pieces.v(turn).pieceUpdated(king.move(newKingPosition)).pieceUpdated(rook.move(newRookPosition)),
                turn.other() to pieces.v(turn.other())
            ),
            bitboard.bitUpdated(Move(king, newKingPosition)).bitUpdated(Move(rook, newRookPosition)),
            status, updatedFiftyMoveRule(king, null)
        )
    }

    private fun pawnPromoted(move: Move): Board {
        val (pawn: Piece, to: Position) = move
        val promoted = pawn.move(to)
        Log.inf("pawnPromoted($pawn, $promoted).")
        val striked = strikedPieceAt(promoted.p)
        return Board(turn.other(),this,
            mapOf(
                turn to pieces.v(turn).pieceUpdated(promoted),
                turn.other() to pieces.v(turn.other()).pieceRemoved(striked)
            ),
            bitboard.bitRemoved(pawn, pawn.side)
                .bitUpdated(Move(promoted, promoted.p))
                .bitRemoved(striked, turn.other()),
            status, updatedFiftyMoveRule(pawn, striked)
        )
    }

    private fun updatedFiftyMoveRule(piece: Piece, striked: Piece?): Int {
        return if ((piece.family == PFamily.PAWN) or (striked != null)) 0
        else fiftyMoveRule + 1
    }

    private fun regularMoved(m: Move): Board {
        val movedPiece = m.piece.move(m.to)
        val striked = strikedPieceAt(movedPiece.p)
        return Board(turn.other(), this,
            mapOf(
                turn to pieces.v(turn).pieceUpdated(movedPiece),
                turn.other() to pieces.v(turn.other()).pieceRemoved(striked)
            ),
            bitboard.bitRemoved(striked, turn.other()).bitUpdated(m), status, updatedFiftyMoveRule(m.piece, striked)
        )
    }

    private fun isPromotion(m: Move): Boolean {
        return ((if (m.piece.side==Side.B) 0b00000000_00000000_00000000_00000000_00000000_00000000_11111111_00000000uL else 0b00000000_11111111_00000000_00000000_00000000_00000000_00000000_00000000uL) and bitboard.side.v(m.piece.side).pawns and (START shr m.piece.p.bitboardIndex())) > 0uL
//        val movedPiece = m.piece.move(m.to)
//        return (m.piece.side == movedPiece.side) and (m.piece.family == PFamily.PAWN) and (movedPiece.family == PFamily.QUEEN)
    }

    private fun isCastling(pType: PType, m: Move): Boolean {
        val rook = pieces.v(turn)[pType] ?: return false
        return (!m.piece.moved) and (!rook.moved)  and (m.to == rook.p) and (m.piece.family == PFamily.KING)
    }

    fun makeMove(move: Move) = makeMove(move, true)
    fun makeMove(move: Move, withUpdatedStatus: Boolean): Board {
        val newBoard = when {
            isCastling(PType.ROOK_L, move) -> castled(PType.ROOK_L, 'L')
            isCastling(PType.ROOK_R, move) -> castled(PType.ROOK_R, 'R')
            isPromotion(move) -> pawnPromoted(move)
            else -> regularMoved(move)
        }
        // we don't even think about accepting moves that result in own kings check
        if (newBoard.getKing(turn).inCheck(newBoard)) {
            return Board(turn, predecessor, pieces, bitboard, ChessStatus.INVALID, fiftyMoveRule)
        }
        return if (withUpdatedStatus)
            newBoard.withUpdatedStatus()
        else
            newBoard
    }
    fun isInvalid() = status == ChessStatus.INVALID
    fun isReadyToEval(): Boolean = (status == ChessStatus.CHECK_MATE) or (status == ChessStatus.STALE_MATE) or (status == ChessStatus.DRAW) or (status == ChessStatus.FIFTY_MOVE)
    fun eval(): Int = Evaluator.eval(this)
    fun allMoves() = pieces.v(turn).values.flatMap { piece -> piece.allMoves(this).map { pos -> Move(piece, pos) } }
    fun allLegalMoves() = allMoves().filter { !makeMove(it, false).isInvalid() }
    fun allLegalSubPositions() = allMoves().map { m -> m to makeMove(m, true) }.filter { !it.second.isInvalid() }
    private fun askStillMovesToMake() = allLegalMoves().isNotEmpty()
    fun Map<PType, Piece>.pieceUpdated(piece: Piece?) = if (piece == null) this else this.mapValues { if (it.key == piece.searchKey) piece else it.value }
    fun Map<PType, Piece>.pieceRemoved(piece: Piece?) = if (piece == null) this else minus(piece.searchKey)
    fun predecessor(): Board = this.predecessor ?: this
    fun getKing(turn: Side = this.turn) = (pieces.v(turn).v(PType.KING) as King)
    fun isOver() = (status == ChessStatus.CHECK_MATE) or (status == ChessStatus.STALE_MATE) or (status == ChessStatus.DRAW) or (status == ChessStatus.FIFTY_MOVE)
    fun whoAt(p: Position) = pieces.v(turn.other()).values.find { piece -> piece.p == p } ?: pieces.v(turn).values.find { piece -> piece.p == p }
}
