

class Node(val position: Board, val father: Node?, val move: Move?, val depth: Int, val score: Int, val siblings: List<Node> = emptyList()) {

    data class StoredInformation(val depth: Int, val score: Int/*, val from: Position, val to: Position*/)

    companion object {

        fun choose(maximizing: Boolean, node1: Node, node2: Node): Node {
            return when(maximizing) {
                true -> Node.max(node1, node2)
                false -> Node.min(node1, node2)
            }
        }
        fun max(node1: Node, node2: Node): Node {
            val (first, sec) = when {
                node1.score>node2.score -> node1 to null
                node2.score>node1.score -> node2 to null
                node1.depth<node2.depth -> node1 to null
                node2.depth<node1.depth -> node2 to null
                else -> node1 to node2
            }
            return Node(first.position, first.father, (first.father?.move ?: first.move), first.depth, first.score, first.siblings.plusMaybeElement(sec, true/*sec?.father?.isRoot */))
        }
        fun min(node1: Node, node2: Node): Node {
            val (first, sec) = when {
                node1.score<node2.score -> node1 to null
                node2.score<node1.score -> node2 to null
                node1.depth<node2.depth -> node1 to null
                node2.depth<node1.depth -> node2 to null
                else -> node1 to node2
            }
            return Node(first.position, first.father, (first.father?.move ?: first.move), first.depth, first.score, first.siblings.plusMaybeElement(sec, true/*sec?.father?.isRoot ?: false*/))
        }
    }
    val isRoot: Boolean
    get() = (father==null)
    val maximizing: Boolean
    get() = (position.turn==Side.B)

    //fun children() = position.allLegalMoves().map { m -> Node(this.position.makeMove(m, true), this, m, this.depth+1, if (maximizing) Int.MAX_VALUE else Int.MIN_VALUE) }
    fun children() = position.allLegalSubPositions().map { (m, subPosition) -> Node(subPosition, this, m, this.depth+1, if (maximizing) Int.MAX_VALUE else Int.MIN_VALUE) }
    fun combined() = siblings.plusElement(this).filter { (it.move?.piece?.side==this.position.turn&&move!=null)/*&&it.isRoot*/ }
    fun eval() = Node(father?.position?:position, father?.father, father?.move?:move, depth, position.eval())
    override fun toString() = "isRoot=$isRoot, depth=$depth, score=$score, move=$move"
}

val nodesMemo : Map<Side, HashMap<Long,Node.StoredInformation>> = ChessAIWithNodes.read()


object ChessAIWithNodes {

    fun persist() = ChessPersistence<Long, Node.StoredInformation>().persist { "${it.key};${it.value.depth};${it.value.score}" }
    fun read() = ChessPersistence<Long, Node.StoredInformation>().read {
        val (key, depth, score/*, from, to*/) = it.split(";")
        ChessPersistence.PersistItem(key.toLong(), Node.StoredInformation(depth.toInt(), score.toInt()/*, Position.fromString(from), Position.fromString(to)*/))
    }

    fun traverse(father: Node, depth: Int=3): Node {
        if (father.position.isReadyToEval()) return father.eval()
        if (father.depth==depth) return father.eval()
        val stored = nodesMemo.v(father.position.turn)[father.position.zobristHash]
        if (stored!=null) {
            if (father.depth>0&&father.depth==stored.depth) {
                return Node(father.position, father.father, null, stored.depth, stored.score, emptyList())
            }
        }
        val n = father.children().map { traverse(it, depth) }.reduce { n1, n2 -> Node.choose(father.maximizing, n1, n2) }

        nodesMemo.v(father.position.turn)[father.position.zobristHash] = Node.StoredInformation(n.depth, n.score)
        return Node(father.position, father.father, n.move, n.depth, n.score, n.siblings)
    }


fun getMove(position: Board, depth: Int=3): Node {
        val startNode = Node(position, null, null, 0, if (position.turn==Side.B) Int.MIN_VALUE else Int.MAX_VALUE)
        val move = traverse(startNode, depth)
//        Log.tmp("depth $depth")
//        Log.tmp("myMove: ${move.move}")
//        move.combined().forEach { Log.tmp("combined:: $it") }
//        Log.tmp("----> ${move.move}")
        return move.combined().randomOrNull() ?:
        Node(position, null, position.allLegalMoves().random(), 0,0)
    }

}