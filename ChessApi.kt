
class ChessApi {

    var board = Board.create()
        private set
    var isMoving = false
        private set
    var selectedPiece: Piece? = null
        private set
    var lastCPUsMoveFindTime = 0f
        private set
    var lastCPUsMove: Move? = null
        private set
    var cpuDepth: Int = 3
    fun selectPiece(x: String, y: String) {
    //if (isMoving) return
        val who = board.whoAt(Position(x.toInt(), y.toInt())) ?: return
        selectedPiece = who
    }

    fun replaceBoard(board: Board) { this.board = board }
    fun resetBoard() {
        replaceBoard(Board.create())
        this.isMoving=false
        this.selectedPiece=null
        this.lastCPUsMove=null
        this.lastCPUsMoveFindTime=0f
    }
    fun apiRevokeMove() {
        isMoving = false
        selectedPiece = null
        board = board.predecessor()
    }
    fun apiHtmlRepr(movesToShow: List<Position> = emptyList(), additionalParameters: Map<String, String> = emptyMap()): Map<String, Any> = Html.apiHtmlRepr(board, movesToShow, additionalParameters)
    fun apiMoves4Piece(): List<Position> = apiMoves4Piece(selectedPiece!!.p.x, selectedPiece!!.p.y)
    fun apiMoves4Piece(x: String, y: String) = apiMoves4Piece(x.toInt(), y.toInt())
    fun apiMoves4Piece(x: Int, y: Int): List<Position> {
        val who = board.whoAt(Position(x,y)) ?: return emptyList()
        return who.allMoves(board).filter { !board.makeMove(Move(who, Position(it.x, it.y))).isInvalid() }
    }

    fun apiMakeMove(x: String, y: String): Board {
        if (isMoving) return board
        if (board.isOver()) return board
        if (selectedPiece==null) return board
        isMoving=true
        val to = Position(x.toInt(), y.toInt())
        return board.makeMove(Move(selectedPiece!!, to), true).also {
            this.board = it
            this.isMoving=false
        }
    }

    fun apiRandomlyPlayedBoard(board: Board = Board.create(), f: (board: Board) -> Unit)  {
        this.board = board
        f(this.board)
        if (board.isOver()) return
        apiRandomlyPlayedBoard(MoveGenerator.randomMoved(board), f)
    }

    fun apiMakeRandomMove(): Board {
        if (board.isOver()) return board
        isMoving=true
        return MoveGenerator.randomMoved(board).also {
            this.board = it
            isMoving=false
        }
    }
    fun apiMakeBestMove(): Triple<Board, Float, Move?> {
        if (board.isOver()) return Triple(board, 0f, null)
        isMoving=true
        val (m, executionTime)  = board.bestMove(cpuDepth)
        return (if (m==null)
            Triple(board.withUpdatedStatus(), executionTime , null).also { this.board=it.first }
        else Triple(board.makeMove(m, true), executionTime, m)).also {
            this.board = it.first
            this.lastCPUsMoveFindTime = it.second
            if (m!=null) this.lastCPUsMove = m
            this.isMoving=false
        }
    }
}