enum class PFamily { PAWN, ROOK, BISHOP, KNIGHT, QUEEN, KING }
enum class PType {
    PAWN_0, PAWN_1, PAWN_2, PAWN_3, PAWN_4, PAWN_5, PAWN_6, PAWN_7,
    ROOK_L, BISHOP_L, KNIGHT_L, KING, QUEEN, KNIGHT_R, BISHOP_R, ROOK_R
}

val INIT_DATA_OF_PIECES = mapOf(
    Side.B to mapOf(
        PType.ROOK_L to   Rook(Side.B, Position(0,0), Images.B.ROOK, false, PType.ROOK_L, PFamily.ROOK),
        PType.KNIGHT_L to Knight(Side.B, Position(1,0), Images.B.KNIGHT, false, PType.KNIGHT_L, PFamily.KNIGHT),
        PType.BISHOP_L to Bishop(Side.B, Position(2,0), Images.B.BISHOP, false, PType.BISHOP_L, PFamily.BISHOP),
        PType.QUEEN to    Queen(Side.B, Position(3,0), Images.B.QUEEN, false, PType.QUEEN, PFamily.QUEEN),
        PType.KING to     King(Side.B, Position(4,0), Images.B.KING, false, PType.KING, PFamily.KING),
        PType.BISHOP_R to Bishop(Side.B, Position(5,0), Images.B.BISHOP, false, PType.BISHOP_R, PFamily.BISHOP),
        PType.KNIGHT_R to Knight(Side.B, Position(6,0), Images.B.KNIGHT, false, PType.KNIGHT_R, PFamily.KNIGHT),
        PType.ROOK_R to   Rook(Side.B, Position(7,0), Images.B.ROOK, false, PType.ROOK_R, PFamily.ROOK),
        PType.PAWN_0 to   Pawn(Side.B, Position(0,1), Images.B.PAWN, false, PType.PAWN_0, PFamily.PAWN),
        PType.PAWN_1 to   Pawn(Side.B, Position(1,1), Images.B.PAWN, false, PType.PAWN_1, PFamily.PAWN),
        PType.PAWN_2 to   Pawn(Side.B, Position(2,1), Images.B.PAWN, false, PType.PAWN_2, PFamily.PAWN),
        PType.PAWN_3 to   Pawn(Side.B, Position(3,1), Images.B.PAWN, false, PType.PAWN_3, PFamily.PAWN),
        PType.PAWN_4 to   Pawn(Side.B, Position(4,1), Images.B.PAWN, false, PType.PAWN_4, PFamily.PAWN),
        PType.PAWN_5 to   Pawn(Side.B, Position(5,1), Images.B.PAWN, false, PType.PAWN_5, PFamily.PAWN),
        PType.PAWN_6 to   Pawn(Side.B, Position(6,1), Images.B.PAWN, false, PType.PAWN_6, PFamily.PAWN),
        PType.PAWN_7 to   Pawn(Side.B, Position(7,1), Images.B.PAWN, false, PType.PAWN_7, PFamily.PAWN)
    ),
    Side.W to mapOf(
        PType.ROOK_L to   Rook(Side.W, Position(0,7), Images.W.ROOK, false, PType.ROOK_L, PFamily.ROOK),
        PType.KNIGHT_L to Knight(Side.W, Position(1,7), Images.W.KNIGHT, false, PType.KNIGHT_L, PFamily.KNIGHT),
        PType.BISHOP_L to Bishop(Side.W, Position(2,7), Images.W.BISHOP, false, PType.BISHOP_L, PFamily.BISHOP),
        PType.QUEEN to    Queen(Side.W, Position(3,7), Images.W.QUEEN, false, PType.QUEEN, PFamily.QUEEN),
        PType.KING to     King(Side.W, Position(4,7), Images.W.KING, false, PType.KING, PFamily.KING),
        PType.BISHOP_R to Bishop(Side.W, Position(5,7), Images.W.BISHOP, false, PType.BISHOP_R, PFamily.BISHOP),
        PType.KNIGHT_R to Knight(Side.W, Position(6,7), Images.W.KNIGHT, false, PType.KNIGHT_R, PFamily.KNIGHT),
        PType.ROOK_R to   Rook(Side.W, Position(7,7), Images.W.ROOK, false, PType.ROOK_R, PFamily.ROOK),
        PType.PAWN_0 to   Pawn(Side.W, Position(0,6), Images.W.PAWN, false, PType.PAWN_0, PFamily.PAWN),
        PType.PAWN_1 to   Pawn(Side.W, Position(1,6), Images.W.PAWN, false, PType.PAWN_1, PFamily.PAWN),
        PType.PAWN_2 to   Pawn(Side.W, Position(2,6), Images.W.PAWN, false, PType.PAWN_2, PFamily.PAWN),
        PType.PAWN_3 to   Pawn(Side.W, Position(3,6), Images.W.PAWN, false, PType.PAWN_3, PFamily.PAWN),
        PType.PAWN_4 to   Pawn(Side.W, Position(4,6), Images.W.PAWN, false, PType.PAWN_4, PFamily.PAWN),
        PType.PAWN_5 to   Pawn(Side.W, Position(5,6), Images.W.PAWN, false, PType.PAWN_5, PFamily.PAWN),
        PType.PAWN_6 to   Pawn(Side.W, Position(6,6), Images.W.PAWN, false, PType.PAWN_6, PFamily.PAWN),
        PType.PAWN_7 to   Pawn(Side.W, Position(7,6), Images.W.PAWN, false, PType.PAWN_7, PFamily.PAWN)
    )
)
