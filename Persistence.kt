import java.io.File


object Serializer {
    fun <T>write(path: String, data: Collection<T>, format: (t: T) -> String) = File(path).writeText(data.map(format).joinToString("\n"))
    fun <T>append(path: String, data: Collection<T>, format: (t: T) -> String) = File(path).appendText(data.map(format).joinToString("\n"))
    fun <T>read(path: String, format: (s: String) -> T) = File(path).readLines().map { format(it) }
}

class ChessPersistence<K, T> {
    val data : Map<Side, HashMap<K,T>> = mapOf(
        Side.B to hashMapOf<K, T>(),
        Side.W to hashMapOf<K, T>()
    )
    val memoSideB = "memob"
    val memoSideW = "memow"
    data class PersistItem<K, T>(val key: K, val value: T)
    fun persist(format: (t: PersistItem<K,T>) -> String) {
        Serializer.append(memoSideB, data.v(Side.B).entries.map { PersistItem(it.key, it.value) })  { format(it) }
        Serializer.append(memoSideW, data.v(Side.W).entries.map { PersistItem(it.key, it.value) })  { format(it) }
    }
    fun <T>read(format: (str: String) -> PersistItem<K, T>): Map<Side, HashMap<K, T>> {
        return try {
            val memoB= Serializer.read(memoSideB) { format(it) }
            val memoW= Serializer.read(memoSideB) { format(it) }
            mapOf(Side.B to hashMapOf(*memoB.map { Pair(it.key,it.value) }.toTypedArray()), Side.W to hashMapOf(*memoW.map { Pair(it.key,it.value) }.toTypedArray()))
        } catch (e: java.io.FileNotFoundException) { mapOf(Side.B to hashMapOf(), Side.W to hashMapOf()) }
    }
}