import LookupTables.LOG2 as LOG2

object MoveGenerator {

    fun randomMoved(board: Board): Board {
        val move = giveRandomMove(board)
        return if (move == null) {
            println("randomMoved($board). move == null")
            board.withUpdatedStatus()
        } else {
            board.makeMove(move, true)
        }
    }

    private fun giveRandomMove(board: Board): Move? {
        val moves = board.allLegalMoves()
        if (moves.isEmpty()) return null
        return moves.random()
    }



    fun setCellState(pFrom: Position, pTo: Position, bitBoard: ULong): ULong {
        val mIndex = (pTo.y * 8) + pTo.x
        val mIndexFrom = (pFrom.y * 8) + pFrom.x
        val newBit = START shr mIndex
        val oldBit = START shr mIndexFrom
        return (bitBoard and oldBit.inv()) or newBit // we remove oldBit from bitboard and add newBit
    }

    fun removeCell(p: Position, bitBoard: ULong): ULong {
        val mIndex = p.y * 8 + p.x
        val removeBit = START shr mIndex
        return (bitBoard and removeBit.inv())
    }

    // representing a position on board after the one bit got shifted right
    const val START = 0b10000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000uL

    fun forQueen(p: Position, opponentPositions: ULong, friendPositions: ULong): ULong {
        return forRook(p, opponentPositions, friendPositions) or
                forBishop(p, opponentPositions, friendPositions)
    }

    fun forBishop(p: Position, opponentPositions: ULong, friendPositions: ULong): ULong {
        val mIndex = p.y * 8 + p.x
        val UNDEFINED = -2147483648

        //
        //
        // until opponent
        var possiblesMovesLL = LookupTables.bishop[mIndex]!![2]!! // Left Left
        var hb = (possiblesMovesLL and opponentPositions).takeHighestOneBit()

        if (LOG2.v(hb) != UNDEFINED) // prevent log2 dangers
            possiblesMovesLL = (possiblesMovesLL shr LOG2.v(hb)) shl (LOG2.v(hb))

        var possiblesMovesLR = LookupTables.bishop[mIndex]!![3]!! // Left Right
        hb = (possiblesMovesLR and opponentPositions).takeHighestOneBit()

        if (LOG2.v(hb) != UNDEFINED) // prevent log2 dangers
            possiblesMovesLR = (possiblesMovesLR shr LOG2.v(hb)) shl (LOG2.v(hb))

        var possiblesMovesUL = LookupTables.bishop[mIndex]!![0]!! // Upper Left
        var lb = (possiblesMovesUL and opponentPositions).takeLowestOneBit()


        if (LOG2.v(lb) != UNDEFINED) // prevent log2 dangers
            possiblesMovesUL = (possiblesMovesUL shl (63-LOG2.v(lb)) shr (63-LOG2.v(lb)))

        var possiblesMovesUR = LookupTables.bishop[mIndex]!![1]!! //Upper Right
        lb = (possiblesMovesUR and opponentPositions).takeLowestOneBit()

        if (LOG2.v(lb) != UNDEFINED) // prevent log2 dangers
            possiblesMovesUR = (possiblesMovesUR shl (63-LOG2.v(lb)) shr (63-LOG2.v(lb)))

        //
        //
        // until friend
        hb = (possiblesMovesLL and friendPositions).takeHighestOneBit() shl 7 /* *** * means: friendly pieces not included :)*/
        if (LOG2.v(hb) != UNDEFINED) // prevent log2 dangers
            possiblesMovesLL = (possiblesMovesLL shr LOG2.v(hb)) shl (LOG2.v(hb))

        hb = (possiblesMovesLR and friendPositions).takeHighestOneBit() shl 9 /* *** * means: friendly pieces not included :)*/
        if (LOG2.v(hb) != UNDEFINED) // prevent log2 dangers
            possiblesMovesLR = (possiblesMovesLR shr LOG2.v(hb)) shl (LOG2.v(hb))

        lb = (possiblesMovesUL and friendPositions).takeLowestOneBit() shr 9 /* *** * means: friendly pieces not included :)*/
        if (LOG2.v(lb) != UNDEFINED) // prevent log2 dangers
            possiblesMovesUL = (possiblesMovesUL shl (63-LOG2.v(lb)) shr (63-LOG2.v(lb)))

        lb = (possiblesMovesUR and friendPositions).takeLowestOneBit() shr 7 /* *** * means: friendly pieces not included :)*/
        if (LOG2.v(lb) != UNDEFINED) // prevent log2 dangers
            possiblesMovesUR = (possiblesMovesUR shl (63-LOG2.v(lb)) shr (63-LOG2.v(lb)))

        return possiblesMovesUL or possiblesMovesUR or possiblesMovesLL or possiblesMovesLR
    }

    fun forKnight(p: Position, freePositions: ULong, opponentPositions: ULong): ULong {
        val mIndex = p.y * 8 + p.x
        val bIndex = START shr mIndex
        val possibleMoves = LookupTables.knight.v(bIndex)
        return (possibleMoves and freePositions) or (possibleMoves and opponentPositions)
    }

    // we decided here that black starts ALWAYS up, and white ALWAYS down
    // if we want to show opposite, representation would be different subject,
    // and must be dealt with elsewhere.
    val kingCastling =
    hashMapOf(           // first := king     , second := rook
            Side.W to mapOf(
                'L' to Pair(Position(2, 7), Position(3, 7)),
                'R' to Pair(Position(6, 7), Position(5, 7))
            ),
            Side.B to mapOf(
                'L' to Pair(Position(2, 0), Position(3, 0)),
                'R' to Pair(Position(6, 0), Position(5, 0))
            )
    )

    data class Promotion(val isAllowed: Boolean, val piece: Piece)
    fun ask4PawnPromotion(pawn: Pawn): Promotion {
        if (pawn.side == Side.W) { // we promote always to queen for now for simplicities sake.
            if (pawn.p.y == 0) return Promotion(
                true,
                Queen(
                    pawn.side,
                    pawn.p,
                    Images.W.QUEEN,
                    true,
                    pawn.searchKey,
                    PFamily.QUEEN
                )
            )
            return Promotion(false, pawn)
        } else {
            if (pawn.p.y == 7) return Promotion(
                true,
                Queen(
                    pawn.side,
                    pawn.p,
                    Images.B.QUEEN,
                    true,
                    pawn.searchKey,
                    PFamily.QUEEN
                )
            )
            return Promotion(false, pawn)
        }
    }
// neu
    fun inCheck(king: King, bitBoard: BitBoard): Boolean {
//        Log.INFO("MoveGenerator.leavesKinginCheck($king, $bitBoard).")
        val opponentsBitBoard = bitBoard.get4Side(king.side.other())
        val friendBitBoard = bitBoard.get4Side(king.side)

        val mIndex = (king.p.y * 8) + king.p.x
        val bIndex = (START shr mIndex)
        // check against knights
        val againstKnights = (LookupTables.knight.v(bIndex) and opponentsBitBoard.knights)
        // check against pawns
        val againstPawns = (LookupTables.pawnSimple.v(king.side).v(bIndex) and opponentsBitBoard.pawns)
        // for rooks bishops and queens we pretend the king to be a such piece
        // this way we can use the implementation for move generation
        // if resulting bitboard contains bits set to 1, we have an intersection so that piece is setting king in check
        // check against rooks
        //                          pOrigin, BB.opponentPosition, BB.friendPosition
        val rookPositions =
            forRook(king.p, opponentsBitBoard.all, friendBitBoard.all)

        val againstRooks = (rookPositions and opponentsBitBoard.rooks)
        // check against bishops
        val bishopPositions =
            forBishop(king.p, opponentsBitBoard.all, friendBitBoard.all)
        val againstBishops = (bishopPositions and opponentsBitBoard.bishops)
        // check against queens
        val againstQueens = (bishopPositions or rookPositions) and opponentsBitBoard.queens
        // check against king
        val kingPositions = forKing(king.p, bitBoard, king.side.other())
        val againstKing = (kingPositions and opponentsBitBoard.king)
        // true: we have an intersection with king. it's check.
        // false: we have none. valid move.     if (againstRooks > 0uL) Log.tmp("leavesKinginCheck against Rooks")
        if (againstBishops > 0uL) Log.tmp("leavesKinginCheck against Bishops")
        if (againstKnights > 0uL) Log.tmp("leavesKinginCheck against Knights")
        if (againstQueens > 0uL) Log.tmp("leavesKinginCheck against Queens")
        if (againstPawns > 0uL) Log.tmp("leavesKinginCheck against Pawns")
        if (againstKing > 0uL) Log.tmp("leavesKinginCheck against King")
    Log.tmp("king @ ${king.p}")

        return (againstKnights or againstPawns or againstRooks or againstBishops or againstQueens or againstKing) > 0uL
    }

     fun forKing(p: Position, bitBoard: BitBoard, side: Side): ULong {
        val mIndex = p.y * 8 + p.x
        val bIndex = START shr mIndex
        val opponentBitBoard = bitBoard.get4Side(side.other())

        val possiblesMoves =
            ((bIndex and Masks.COL0_TO_COL6) shr 1) or// right !!! king may not be in last col
            ((bIndex and Masks.COL1_TO_COL7) shl 1) or// left !!! king may not be in first col
            ((bIndex and Masks.ROW0_TO_ROW6) shr 8) or// down !!! king may not be in last row
            ((bIndex and Masks.ROW1_TO_ROW7) shl 8) or// up !!! king may not be in first row
            ((bIndex and Masks.COL0_TO_COL6 and Masks.ROW1_TO_ROW7) shl 7) or// up-right !!! king may not be in last col and not in first row
            ((bIndex and Masks.COL1_TO_COL7 and Masks.ROW1_TO_ROW7) shl 9) or// up-left !!! king may not be in first col and not in first row
            ((bIndex and Masks.COL0_TO_COL6 and Masks.ROW0_TO_ROW6) shr 9) or// down-right !!! king may not be in last col and not in last row
            ((bIndex and Masks.COL1_TO_COL7 and Masks.ROW0_TO_ROW6) shr 7)   // down-left !!! king may not be in first col and not in last row
        return ((possiblesMoves and bitBoard.free) or (possiblesMoves and opponentBitBoard.all)) and  // !! NEU !! extra check against king from oppoent
                (((opponentBitBoard.king and Masks.COL0_TO_COL6) shr 1) or// right !!! king may not be in last col
                ((opponentBitBoard.king and Masks.COL1_TO_COL7) shl 1) or// left !!! king may not be in first col
                ((opponentBitBoard.king and Masks.ROW0_TO_ROW6) shr 8) or// down !!! king may not be in last row
                ((opponentBitBoard.king and Masks.ROW1_TO_ROW7) shl 8) or// up !!! king may not be in first row
                ((opponentBitBoard.king and Masks.COL0_TO_COL6 and Masks.ROW1_TO_ROW7) shl 7) or// up-right !!! king may not be in last col and not in first row
                ((opponentBitBoard.king and Masks.COL1_TO_COL7 and Masks.ROW1_TO_ROW7) shl 9) or// up-left !!! king may not be in first col and not in first row
                ((opponentBitBoard.king and Masks.COL0_TO_COL6 and Masks.ROW0_TO_ROW6) shr 9) or// down-right !!! king may not be in last col and not in last row
                ((opponentBitBoard.king and Masks.COL1_TO_COL7 and Masks.ROW0_TO_ROW6) shr 7)).inv()   // down-left !!! king may not be in first col and not in last row).inv()
    }



    fun forPawn(p: Position, side: Side, moved: Boolean, freePositions: ULong, opponentPositions: ULong): ULong {
        val mIndex = p.y * 8 + p.x
        val bIndex = START shr mIndex
        val possiblesMoves = if (side == Side.B) (bIndex shr 8) /* down*/  else (bIndex shl 8) /* up */

        val possibleCaptureMoves = if (side == Side.B) {
            (((bIndex and Masks.COL0_TO_COL6) shr 9) and opponentPositions) or // kill down right if col not last
            (((bIndex and Masks.COL1_TO_COL7) shr 7 ) and opponentPositions) // kill down left if col not first
        } else {
            (((bIndex and Masks.COL0_TO_COL6) shl 7) and opponentPositions) or // kill up right if col not last
            (((bIndex and Masks.COL1_TO_COL7) shl 9) and opponentPositions) // kill up left if col not first
        }

        val pp = (freePositions and possiblesMoves) or (opponentPositions and possibleCaptureMoves)

        return if (moved) {
            pp
        } else {
            if (side == Side.B) {
                if (pp == 0uL) pp
                else pp or (freePositions and (START shr (mIndex + 16))) // 2 down
            } else {
                if (pp == 0uL) pp
                else pp or (freePositions and (START shr (mIndex - 16))) // 2 up
            }
        }
    }



    fun forRook(p: Position, opponentPositions: ULong, friendPositions: ULong): ULong {
        val mIndex = p.y * 8 + p.x
        val UNDEFINED = -2147483648
        //
        //
        // until opponent
        var possiblesMovesD = LookupTables.rook[mIndex]!!['D']!!
        var hb = (possiblesMovesD and opponentPositions).takeHighestOneBit()
        if (LOG2.v(hb) != UNDEFINED) // prevent log2 dangers
            possiblesMovesD = (possiblesMovesD shr LOG2.v(hb)) shl (LOG2.v(hb))

        var possiblesMovesR = LookupTables.rook[mIndex]!!['R']!!
        hb = (possiblesMovesR and opponentPositions).takeHighestOneBit()
        if (LOG2.v(hb) != UNDEFINED) // prevent log2 dangers
            possiblesMovesR = (possiblesMovesR shr LOG2.v(hb)) shl (LOG2.v(hb))

        var possiblesMovesL = LookupTables.rook[mIndex]!!['L']!!
        var lb = (possiblesMovesL and opponentPositions).takeLowestOneBit()
        if (LOG2.v(lb) != UNDEFINED) // prevent log2 dangers
        possiblesMovesL = (possiblesMovesL shl (63-LOG2.v(lb)) shr (63-LOG2.v(lb)))

        var possiblesMovesU = LookupTables.rook[mIndex]!!['U']!!
        lb = (possiblesMovesU and opponentPositions).takeLowestOneBit()
        if (LOG2.v(lb) != UNDEFINED) // prevent log2 dangers
            possiblesMovesU = (possiblesMovesU shl (63-LOG2.v(lb)) shr (63-LOG2.v(lb)))
        //
        //
        // until friend
        hb = (possiblesMovesD and friendPositions).takeHighestOneBit() shl 8 /* shl 8 means: friendly pieces not included :)*/
        if (LOG2.v(hb) != UNDEFINED) // prevent log2 dangers
            possiblesMovesD = (possiblesMovesD shr LOG2.v(hb)) shl (LOG2.v(hb))

        hb = (possiblesMovesR and friendPositions).takeHighestOneBit() shl 1 /* shl 8 means: friendly pieces not included :)*/
        if (LOG2.v(hb) != UNDEFINED) // prevent log2 dangers
            possiblesMovesR = (possiblesMovesR shr LOG2.v(hb)) shl (LOG2.v(hb))

        lb = (possiblesMovesL and friendPositions).takeLowestOneBit() shr 1 /* shl 8 means: friendly pieces not included :)*/
        if (LOG2.v(lb) != UNDEFINED) // prevent log2 dangers
            possiblesMovesL = (possiblesMovesL shl (63-LOG2.v(lb)) shr (63-LOG2.v(lb)))

        lb = (possiblesMovesU and friendPositions).takeLowestOneBit() shr 8 /* shl 8 means: friendly pieces not included :)*/
        if (LOG2.v(lb) != UNDEFINED) // prevent log2 dangers
            possiblesMovesU = (possiblesMovesU shl (63-LOG2.v(lb)) shr (63-LOG2.v(lb)))

        return possiblesMovesD or possiblesMovesR or possiblesMovesL or possiblesMovesU
    }
}