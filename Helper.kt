
fun ULong.toLine() = this.toString(2).padStart(64, '0').chunked(8).joinToString("_")
fun ULong.toMatrix() = this.toString(2).padStart(64, '0').chunked(8).joinToString("\n")
fun <K, V> HashMap<K,V>.v(key: K): V = this[key]!!
fun <K, V> Map<K,V>.v(key: K): V = this[key]!!
// source: https://discuss.kotlinlang.org/t/best-way-to-replace-an-element-of-an-immutable-list/8646/9
fun <E> Iterable<E>.replace(index: Int, elem: E) = mapIndexed { i, existing ->  if (i == index) elem else existing }
fun <T> Iterable<T>.plus(elements: T, predicate: () -> Boolean): List<T> {
    return if (predicate())
        this.plus(elements)
    else
        (this as Collection).toList()
}

fun <T> Collection<T>.plusElement(element: T, predicate: () -> Boolean): List<T> {
    return if (predicate())
        plusElement(element)
    else
        toList()
}

fun <T> Collection<T>.plusElementIfNotNull(element: T?): List<T> {
    return if (element!=null)
        plusElement(element)
    else
        toList()
}

fun <T> Collection<T>.plusElement(element: T, predicate: Boolean): List<T> {
    return if (predicate)
        this.plusElement(element)
    else
        (this as Collection).toList()
}
fun <T> Collection<T>.plusMaybeElement(element: T?, predicate: Boolean): List<T> {
    return if (element!=null&&predicate)
        this.plusElement(element)
    else
        this.toList()
}
fun Boolean.toInt(falseValue: Int = -1) = if (this) 1 else falseValue
// https://stackoverflow.com/questions/16120697/kotlin-how-to-pass-a-function-as-parameter-to-another
infix fun Int.times(f: () -> Unit) { repeat(this) { f() } }


object Log {
    fun inf(vararg msg: String) = if (false) println("INFO: ${msg.joinToString()}") else Unit
    fun err(vararg msg: String) = if (false) println("ERROR: ${msg.joinToString()}") else Unit
    fun war(vararg msg: String) = if (false) println("WARN: ${msg.joinToString()}") else Unit
    fun tmp(vararg msg: String) = if (false) println("TMP: ${msg.joinToString()}") else Unit
}
